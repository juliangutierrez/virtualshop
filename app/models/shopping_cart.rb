class ShoppingCart < ApplicationRecord
	has_and_belongs_to_many_with_deferred_save :products
	belongs_to :user
	validate :product_uniqueness

	def total_price
		products.sum(&:price)
	end

	def add_product product
		products.push product
		save
	end

	def remove_product product
		products.delete product
		save
	end

	private
	def product_uniqueness
		if products.uniq.length < products.length
			errors.add(:products, 'Your cart already contains the product')
		end
	end
end
