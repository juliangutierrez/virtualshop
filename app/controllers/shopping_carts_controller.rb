class ShoppingCartsController < ApplicationController
	before_action :set_shopping_cart

	def add_product
		current_user.shopping_cart.add_product Product.find(params[:id])
		render 'show'
	end

	def remove_product
		current_user.shopping_cart.remove_product(Product.find params[:id])
		render 'show'
	end

	def show
		
	end

	private
	def set_shopping_cart
		current_user.shopping_cart = ShoppingCart.new unless current_user.shopping_cart.present?
	end
end
