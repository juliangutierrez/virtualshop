# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
hash = JSON.parse(File.read('products.json'))
hash['products'].each do |product|
	Product.create name: product['name'], brand: product['brand'], model: product['model'], sku: product['sku'], price: product['price'], desc: product['desc']
end



