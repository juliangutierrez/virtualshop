Rails.application.routes.draw do
	root 'products#index'
  
  resources :products, only: [:index, :show]
  
  devise_for :users do
	  get '/users/sign_out' => 'devise/sessions#destroy'
	end
	
	get '/shopping_cart' => 'shopping_carts#show', as: 'shopping_cart'
	get '/shopping_cart/add_product/:id' => 'shopping_carts#add_product', as: 'shopping_cart_add_product'
	delete '/shopping_cart/:id' => 'shopping_carts#remove_product', as: 'shopping_cart_remove_product'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
